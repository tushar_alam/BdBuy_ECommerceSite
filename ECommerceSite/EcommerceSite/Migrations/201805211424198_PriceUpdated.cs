namespace EcommerceSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PriceUpdated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prices", "PriceAmount", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Prices", "PriceAmount");
        }
    }
}
