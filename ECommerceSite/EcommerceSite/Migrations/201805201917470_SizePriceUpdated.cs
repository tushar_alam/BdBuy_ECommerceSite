namespace EcommerceSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SizePriceUpdated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prices", "PriceTitle", c => c.String());
            AddColumn("dbo.Sizes", "SizeTitle", c => c.String());
            DropColumn("dbo.Prices", "PriceAmount");
            DropColumn("dbo.Sizes", "SizeNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sizes", "SizeNumber", c => c.Double(nullable: false));
            AddColumn("dbo.Prices", "PriceAmount", c => c.Double(nullable: false));
            DropColumn("dbo.Sizes", "SizeTitle");
            DropColumn("dbo.Prices", "PriceTitle");
        }
    }
}
