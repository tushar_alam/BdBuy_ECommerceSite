namespace EcommerceSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PriceAmountTypeChanged : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Prices", "PriceAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Prices", "PriceAmount", c => c.Double(nullable: false));
        }
    }
}
