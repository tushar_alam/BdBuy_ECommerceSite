namespace EcommerceSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CartProductAndUserInfoAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CartProducts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Image = c.Binary(),
                        UserInfoId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserInfoes", t => t.UserInfoId, cascadeDelete: true)
                .Index(t => t.UserInfoId);
            
            CreateTable(
                "dbo.UserInfoes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        EmailOrPhone = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Gander = c.String(),
                        Profession = c.String(),
                        DateOfBirth = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CartProducts", "UserInfoId", "dbo.UserInfoes");
            DropIndex("dbo.CartProducts", new[] { "UserInfoId" });
            DropTable("dbo.UserInfoes");
            DropTable("dbo.CartProducts");
        }
    }
}
