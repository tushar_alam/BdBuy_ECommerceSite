namespace EcommerceSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserInfoUpdated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserInfoes", "Address", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserInfoes", "Address");
        }
    }
}
