namespace EcommerceSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ColourSizePriceDestinationPlacesAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Colours",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ColourName = c.String(),
                        MasterCategoryId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MasterCategories", t => t.MasterCategoryId, cascadeDelete: true)
                .Index(t => t.MasterCategoryId);
            
            CreateTable(
                "dbo.DestinationPlaces",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        MasterCategoryId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MasterCategories", t => t.MasterCategoryId, cascadeDelete: true)
                .Index(t => t.MasterCategoryId);
            
            CreateTable(
                "dbo.Prices",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PriceAmount = c.Double(nullable: false),
                        MasterCategoryId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MasterCategories", t => t.MasterCategoryId, cascadeDelete: true)
                .Index(t => t.MasterCategoryId);
            
            CreateTable(
                "dbo.Sizes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SizeNumber = c.Double(nullable: false),
                        MasterCategoryId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MasterCategories", t => t.MasterCategoryId, cascadeDelete: true)
                .Index(t => t.MasterCategoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sizes", "MasterCategoryId", "dbo.MasterCategories");
            DropForeignKey("dbo.Prices", "MasterCategoryId", "dbo.MasterCategories");
            DropForeignKey("dbo.DestinationPlaces", "MasterCategoryId", "dbo.MasterCategories");
            DropForeignKey("dbo.Colours", "MasterCategoryId", "dbo.MasterCategories");
            DropIndex("dbo.Sizes", new[] { "MasterCategoryId" });
            DropIndex("dbo.Prices", new[] { "MasterCategoryId" });
            DropIndex("dbo.DestinationPlaces", new[] { "MasterCategoryId" });
            DropIndex("dbo.Colours", new[] { "MasterCategoryId" });
            DropTable("dbo.Sizes");
            DropTable("dbo.Prices");
            DropTable("dbo.DestinationPlaces");
            DropTable("dbo.Colours");
        }
    }
}
