namespace EcommerceSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CartModelUpdated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CartProducts", "Qty", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CartProducts", "Qty");
        }
    }
}
