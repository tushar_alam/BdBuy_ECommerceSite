namespace EcommerceSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TourPackegeAddedUpdated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TourPackeges", "Image", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TourPackeges", "Image");
        }
    }
}
