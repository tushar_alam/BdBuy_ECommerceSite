namespace EcommerceSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CategoryRelatedToDestinationPlace : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DestinationPlaces", "CategoryId", c => c.Long(nullable: false));
            CreateIndex("dbo.DestinationPlaces", "CategoryId");
            AddForeignKey("dbo.DestinationPlaces", "CategoryId", "dbo.Categories", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DestinationPlaces", "CategoryId", "dbo.Categories");
            DropIndex("dbo.DestinationPlaces", new[] { "CategoryId" });
            DropColumn("dbo.DestinationPlaces", "CategoryId");
        }
    }
}
