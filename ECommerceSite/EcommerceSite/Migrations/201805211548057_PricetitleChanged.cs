namespace EcommerceSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PricetitleChanged : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prices", "PriceRangeName", c => c.String());
            DropColumn("dbo.Prices", "PriceTitle");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Prices", "PriceTitle", c => c.String());
            DropColumn("dbo.Prices", "PriceRangeName");
        }
    }
}
