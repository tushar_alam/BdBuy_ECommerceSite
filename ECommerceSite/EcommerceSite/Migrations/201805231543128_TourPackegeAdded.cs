namespace EcommerceSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TourPackegeAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TourPackeges",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CategoryId = c.Long(nullable: false),
                        DestinationPlaceId = c.Long(nullable: false),
                        Name = c.String(nullable: false),
                        HotelName = c.String(),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.DestinationPlaces", t => t.DestinationPlaceId)
                .Index(t => t.CategoryId)
                .Index(t => t.DestinationPlaceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TourPackeges", "DestinationPlaceId", "dbo.DestinationPlaces");
            DropForeignKey("dbo.TourPackeges", "CategoryId", "dbo.Categories");
            DropIndex("dbo.TourPackeges", new[] { "DestinationPlaceId" });
            DropIndex("dbo.TourPackeges", new[] { "CategoryId" });
            DropTable("dbo.TourPackeges");
        }
    }
}
