namespace EcommerceSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ManAndWomanProductsModelAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MansProducts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BrandId = c.Long(nullable: false),
                        SizeId = c.Long(nullable: false),
                        ColourId = c.Long(nullable: false),
                        Name = c.String(nullable: false),
                        Image = c.Binary(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(maxLength: 1000),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Brands", t => t.BrandId, cascadeDelete: true)
                .ForeignKey("dbo.Colours", t => t.ColourId)
                .ForeignKey("dbo.Sizes", t => t.SizeId)
                .Index(t => t.BrandId)
                .Index(t => t.SizeId)
                .Index(t => t.ColourId);
            
            CreateTable(
                "dbo.WomansProducts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BrandId = c.Long(nullable: false),
                        SizeId = c.Long(nullable: false),
                        ColourId = c.Long(nullable: false),
                        Name = c.String(nullable: false),
                        Image = c.Binary(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(maxLength: 1000),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Brands", t => t.BrandId, cascadeDelete: true)
                .ForeignKey("dbo.Colours", t => t.ColourId)
                .ForeignKey("dbo.Sizes", t => t.SizeId)
                .Index(t => t.BrandId)
                .Index(t => t.SizeId)
                .Index(t => t.ColourId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MansProducts", "SizeId", "dbo.Sizes");
            DropForeignKey("dbo.WomansProducts", "SizeId", "dbo.Sizes");
            DropForeignKey("dbo.WomansProducts", "ColourId", "dbo.Colours");
            DropForeignKey("dbo.WomansProducts", "BrandId", "dbo.Brands");
            DropForeignKey("dbo.MansProducts", "ColourId", "dbo.Colours");
            DropForeignKey("dbo.MansProducts", "BrandId", "dbo.Brands");
            DropIndex("dbo.WomansProducts", new[] { "ColourId" });
            DropIndex("dbo.WomansProducts", new[] { "SizeId" });
            DropIndex("dbo.WomansProducts", new[] { "BrandId" });
            DropIndex("dbo.MansProducts", new[] { "ColourId" });
            DropIndex("dbo.MansProducts", new[] { "SizeId" });
            DropIndex("dbo.MansProducts", new[] { "BrandId" });
            DropTable("dbo.WomansProducts");
            DropTable("dbo.MansProducts");
        }
    }
}
