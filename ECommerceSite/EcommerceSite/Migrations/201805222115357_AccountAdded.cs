namespace EcommerceSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccountAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountId = c.Int(nullable: false, identity: true),
                        EmailOrPhone = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Gander = c.String(),
                        Profession = c.String(),
                        DateOfBirth = c.String(nullable: false),
                        Image = c.Binary(),
                    })
                .PrimaryKey(t => t.AccountId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Accounts");
        }
    }
}
