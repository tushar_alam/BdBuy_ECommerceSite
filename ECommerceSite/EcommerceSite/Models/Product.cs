﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EcommerceSite.Models
{
    public class Product
    {
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        public byte[] Image { get; set; }

        [Required]
        public decimal Price { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(maximumLength: 1000, ErrorMessage = "Description can not be more then 1000 characters")]
        public string Description { get; set; }
        public string Feature { get; set; }

        [Required]
        public long BrandId { get; set; }
        public virtual Brand Brand { get; set; }

        public long? TypeId { get; set; }
        public virtual Type Type { get; set; }
    }
}